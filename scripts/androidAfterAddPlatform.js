/**
 * Simple script that performs some setup before android can be build
 */
var fs = require("fs");
var path = require("path");

module.exports = function(context) {
  fs.createReadStream(path.join(context.opts.plugin.dir,"src","android","google-services.json")).
    pipe(fs.createWriteStream(path.join(context.opts.projectRoot,"platforms","android","google-services.json")));
};
