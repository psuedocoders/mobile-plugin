# Location sync plugin for the Schoolsmart Application

### Requirements
* nodejs

### Setup
* Add the following line to build.gradle under buildscript>dependencies  
in the root android directory (cordova doesn't provide automated additions  
to the gradle build file):  
classpath 'com.google.gms:google-services:3.0.0'