package com.pseudocoders;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by james on 7/10/16.
 */

public class FirebasePrimaryDeviceManager {

    public static final int GUARDIAN_ID_INDEX = 0;

    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if(action.equals("getId")){
            callbackContext.success(FirebaseInstanceId.getInstance().getId());
        } else if(action.equals("setThisDeviceAsPrimary")) {
            try {
                // Set this device as primary
                DatabaseReference guardianRef = FirebaseDatabase.
                        getInstance().
                        getReference().
                        child("users").
                        child(data.getString(FirebasePrimaryDeviceManager.GUARDIAN_ID_INDEX));
                guardianRef.child("primaryDeviceId").setValue(FirebaseInstanceId.getInstance().getId());
            } catch (Exception e) {
                callbackContext.error("Something went wrong setting primary device");
            }
        }

        return true;
    }
}
